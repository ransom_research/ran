import sys
import subprocess

relays = []
f = open(sys.argv[1], 'r')
for line in f:
    relays.append(line.strip())
f.close()

# run 'traceroute <domain>' command on each relay and just scp the country path file here

for r in relays:
    subprocess.Popen(["scp", (r + "_country_paths.txt"), "root@" + r + ":" + r + "_country_paths.txt"]).wait()

