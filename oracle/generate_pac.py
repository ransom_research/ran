import sys
import random

### usage: python generate_pac.py <country_to_avoid> <origin country> <client_domain_paths> <client_relay_paths> <relay_client_paths> <domains_file>

cc_to_avoid = sys.argv[1]
#cc_to_avoid = "United Kingdom"

origin_country = sys.argv[2]

relay_code = {}
f = open('relay_mapping.txt', 'r')
for line in f:
    [ip, code] = line.strip().split(" ")
    relay_code[ip] = code
f.close()

# based on country-level paths, generate a map of <domain -> relay/DIRECT>
dest_map = {}

# first, check client -> domain paths
f = open(sys.argv[3], 'r')
for line in f:
    if cc_to_avoid not in line.split(",")[1:]:
        dest_map[line.split(",")[0]] = ["DIRECT"]
f.close()

# second, check client -> relay paths, and relay -> domain paths
f = open(sys.argv[4], 'r')
forward_relays = []
for line in f:
    if cc_to_avoid not in line.split(",")[1:]:
        forward_relays.append(line.split(",")[0])
f.close()

reverse_relays = []
f = open(sys.argv[5], 'r')
for line in f:
    if cc_to_avoid not in line.split(",")[1:]:
        reverse_relays.append(line.split(",")[0])
f.close()

usable_relays = []
for r in forward_relays:
    if r in reverse_relays:
        usable_relays.append(relay_code[r])

for r in usable_relays:
    f = open(r + "_" + origin_country + "_relay_domains.txt", 'r')
    for line in f:
        if cc_to_avoid not in line.split(",")[1:]:
            if line.split(",")[0] in dest_map:
                dest_map[line.split(",")[0]].append(r)
            else:
                dest_map[line.split(",")[0]] = [r]
    f.close()

f = open(sys.argv[6], 'r')
domains = []
for line in f:
    domains.append(line.strip())
f.close()

# if cc is unavoidable with any relays, then it defaults to DIRECT
for d in domains:
    if not d in dest_map:
        dest_map[d] = ["DIRECT"]

# pick random usable relay for each domain
for y in dest_map:
    if 'DIRECT' in dest_map[y]:
        dest_map[y] = "DIRECT"
    else:
        dest_map[y] = random.choice(dest_map[y])

# convert mapping to <relay/DIRECT -> [domains]
relay_map = {}
for d in dest_map:
    if dest_map[d] in relay_map:
        relay_map[dest_map[d]].append(d)
    else:
        relay_map[dest_map[d]] = [d]

# generate pac file lines
lines = []
lines.append('function FindProxyForURL(url, host){')
for relay in relay_map:
    if relay != "DIRECT":
        s = 'if ((shExpMatch(host, "*' + relay_map[relay][0][3:] + '"))'
        for d in relay_map[relay][1:]:
            s += ' || (shExpMatch(host, "*' + d[3:] + '"))'
        s += ')'
        lines.append(s)
        relay_ip = ''
        for x in relay_code:
            if relay_code[x] == relay:
                relay_ip = x
        lines.append('return "PROXY ' + relay_ip + ':3128";')

# default to direct
lines.append('return "DIRECT";')
lines.append('}')

# write all lines to .pac file
f = open(origin_country + "_" + cc_to_avoid + "_pac.pac", 'w')
for l in lines:
    f.write(l + "\n")
f.close()
