import sys
import util
import time

domains = []
f = open(sys.argv[1], 'r')
for line in f:
    domains.append(line.strip())
f.close()

key = sys.argv[2]

id_map = util.run_traceroute_measurements(domains, "ICMP", key)

time.sleep(300)
trace_map = util.analyze_traceroute_measurements(id_map)

cc_map = util.traceroute_to_countries(trace_map)

f = open(sys.argv[3], 'w')
for c in cc_map:
    for c2 in cc_map[c]:
        f.write(c + "," + ",".join(c2) + "\n")
f.close()
