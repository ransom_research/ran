RAN: Routing Around Nation-States
=============
## Overview
RAN is an overlay network that allows clients to route around countries that may be unfavorable for their policies, such as surveillance or censorship.

## Installation

(These instructions for a client using a Chrome browser)

1) Go to settings (3 bars in the top right corner of the browser).

2) Click "Show advanced settings"

3) Go to "Network" and click "Change Proxy Settings"

4) Click "Automatic Proxy Configuration"

5) Enter the URL that the PAC file is located at

6) Click "OK"

7) Click "Apply"

## Notes

 PAC files are supported by other browsers as well, and therefore clients using non-Chrome browsers can still use RAN.

 Instructions for running relays and oracles are located within their respective directories.
