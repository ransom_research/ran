# utility methods to help run a measurement study

import subprocess
import json
import requests
import re
import time
import geoip2.database

def traceroute_parse(data):
    trace_list = []
    for d in data:
        try:
	    current_trace = []
            for x in d['result']:
                if 'from' in x['result'][0].keys() and 'rtt' in x['result'][0].keys():
                    current_trace.append(str(x['hop']) + ": " + str(x['result'][0]['from']) + " " + str(x['result'][0]['rtt']))
                else:
                    current_trace.append(str(x['hop']) + ": ***")
            trace_list.append(current_trace)
        except:
            f = open('log.txt', 'a')
            f.write('Traceroute analysis failed: ' + str(data) + '\n')
            f.close()
    return trace_list

def get_probes():
    f = open('probes.txt', 'r')
    s = ''
    for line in f:
        s += line.strip() + ','
    f.close()
    return s[:-1]

def get_probes_count():
    f = open('probes.txt', 'r')
    count = 0
    for line in f:
        count += 1
    f.close()
    return count

def check_statuses(measurement_list):
    for m in measurement_list:
        if m.isdigit():
            s = requests.get("https://atlas.ripe.net/api/v1/measurement/" + str(m) + "/?status",)
            data = json.loads(s.text)
            try:
                if data['status']['name'] == 'Failed':
                    return True
                elif data['status']['name'] != 'Stopped':
                    return False
            except:
                return True
    return True

def run_traceroute_measurements(ips, t_type, key):
    measurement_map = {}
    counter = 1
    for ip in ips:
        if counter >= 99:
            time.sleep(900)
            while not check_statuses(measurement_list):
                time.sleep(300)    
            counter = 1
        s = 'curl -H "Content-Type: application/json" -H "Accept: application/json" -X POST -d '
        s += "'{ "
        s += '"definitions": [ { "target": "' + ip.strip() + '", "protocol": "' + t_type + '", "paris": 16, "description": "Traceroute measurement to ' + ip.strip() + '", "type": "traceroute", "af": 4, "is_oneoff": true } ], "probes": [ { "requested": ' + str(get_probes_count()) + ', "type": "probes", "value": "' + get_probes() + '" } ] }'
        s += "' https://atlas.ripe.net/api/v1/measurement/?key=" + key
        response = subprocess.check_output(s, stderr = subprocess.STDOUT, shell=True)
        temp = response.split(":")[-1]
        if ip in measurement_map:
            measurement_map[ip].extend(temp[1:-2].split(","))
        else:
            measurement_map[ip] = temp[1:-2].split(",")
        counter += 1
    return measurement_map

def analyze_traceroute_measurements(id_map):
    traceroute_map = {}
    for domain in id_map:
        for measurement in id_map[domain]:
            if measurement.isdigit():
                x = requests.get("https://atlas.ripe.net/api/v1/measurement/" + str(measurement)  +  "/result/",)
                data = json.loads(x.text)
                if domain in traceroute_map:
                    traceroute_map[domain].extend(traceroute_parse(data))
                else:
                    traceroute_map[domain] = traceroute_parse(data)
    return traceroute_map

def traceroute_to_countries(trace_map):
    cc_map = {}
    reader = geoip2.database.Reader('GeoIP2-City_20160830/GeoIP2-City.mmdb')
    for domain in trace_map:
        for ip_path in trace_map[domain]:
            cc_path = []
            for ip in ip_path:
                # geolocate
                res = reader.city(ip)
                cc = res.country.name
                cc_path.append(cc)
            if domain in cc_map:
                cc_map[domain].append(cc_path)
            else:
                cc_map[domain] = [cc_path]
    return cc_map
