import time
import util
import sys
import subprocess
import re
import geoip2.database

d = sys.argv[1]
p = subprocess.Popen(['traceroute', d], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out,err = p.communicate()
hops = out.split("\n")[1:-1]
ip_hops = []
for h in hops:
	pattern = r"\(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\)"
	pattern_re = re.compile(pattern)
	ip = pattern_re.findall(h.strip())
	if ip != []:
		final_ip = ip[0][1:-1]
		ip_hops.append(final_ip)

reader = geoip2.database.Reader('GeoIP2-City_20160830/GeoIP2-City.mmdb')
cc_hops = []
for ip in ip_hops:
	try:
		res = reader.city(ip)
		cc = res.country.name
		if cc != None:
			cc_hops.append(cc)
	except geoip2.errors.AddressNotFoundError:
		continue

f = open(sys.argv[2], 'w')
s = d
for c in cc_hops:
	s += "," + c
f.write(s + "\n")
f.close()
