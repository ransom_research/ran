import re
import sys
import subprocess
import geoip2.database
import datetime
import time

### usage: python get_paths.py <domain_file> <output_file>

# get all domains
domains = []
f = open(sys.argv[1], 'r')
for line in f:
	domains.append(line.strip())
f.close()

# run traceroutes to all domains and collect IP paths
trace_map = {}
for d in domains:
	p = subprocess.Popen(['traceroute', d], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = p.communicate()
	hops = out.split("\n")[1:-1]
	ip_hops = []
	for h in hops:
		pattern = r"\(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\)"
        	pattern_re = re.compile(pattern)
        	ip = pattern_re.findall(h.strip())
		if ip != []:
			final_ip = ip[0][1:-1]
			ip_hops.append(final_ip)
	trace_map[d] = ip_hops

# convert IP paths to country paths
cc_map = {}
reader = geoip2.database.Reader('GeoIP2-City_20160830/GeoIP2-City.mmdb')
for t in trace_map:
	cc_hops = []
	for ip in trace_map[t]:
		# geolocate
		try:
			res = reader.city(ip)
                	cc = res.country.name
			if cc != None:
				cc_hops.append(cc)
		except geoip2.errors.AddressNotFoundError:
			continue 
	cc_map[t] = cc_hops

# write cc_map to file
f = open(sys.argv[2], 'w')
for c in cc_map:
	f.write(c + "," + ",".join(cc_map[c]) + "\n")
f.close()

